import React from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";

const Header = ({ name, openDrawer }) => (
  <View style={styles.header}>
    <TouchableOpacity onPress={() => openDrawer()}>
      <Ionicons name="ios-menu" size={32} />
    </TouchableOpacity>
    <Text>{name}</Text>
    <Text style={{ width: 50 }}></Text>
  </View>
);

const Forgetpassword = ({ navigation }) => {
  const [resetPass, onChangePass] = React.useState({ value: "" });
  return (
    <View style={styles.container}>
      <Header name="Forget Password" openDrawer={navigation.openDrawer} />
      <View style={styles.childContainer}>
        <Text style={{ fontSize: 20, color: "gray" }}>
          Please enter your email...
        </Text>
        <TextInput
          style={styles.inputText}
          placeholder="JohanDoe@mechanic.com"
          keyboardType="email-address"
          onChangeText={(text) => onChangePass(text)}
          value={resetPass}
        />
        <TouchableOpacity
          style={styles.loginBtn}
          onPress={() => alert(resetPass)}
        >
          <Text style={styles.loginText}>Submit</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Forgetpassword;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    width: "100%",
  },
  childContainer: {
    marginTop: "20%",
    justifyContent: "center",
  },
  inputText: {
    height: 40,
    borderColor: "gray",
    borderWidth: 1.5,
    marginTop: 20,
    paddingLeft: 8,
    paddingRight: 8,
    borderRadius: 5,
  },
  loginBtn: {
    backgroundColor: "#04609F",
    borderRadius: 5,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
  },
  loginText: {
    color: "white",
    fontWeight: "bold",
  },
  header: {
    width: "100%",
    height: 60,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 20,
  },
});
