import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";

class Signup extends Component {
  state = {
    fullname: "",
    email: "",
    phone: "",
    address: "",
    serviceType: "",
    password: "",
  };
  login = (email, pass) => {
    alert("email: " + email + " password: " + pass);
  };
  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity onPress={this.props.navigation.toggleDrawer}>
            <Ionicons name="ios-menu" size={32} />
          </TouchableOpacity>
          <Text>Sign up</Text>
          <Text style={{ width: 50 }}></Text>
        </View>
        <View style={{ padding: 20, marginTop: "20%", width: "100%" }}>
          <Text style={{ fontSize: 20, color: "gray", marginBottom: 20 }}>
            Sign up now
          </Text>
          <TextInput
            style={styles.inputText}
            placeholder="Full Name"
            onChangeText={(text) => this.setState({ fullname: text })}
            value={this.state.fullname}
          />
          <TextInput
            style={styles.inputText}
            placeholder="Email"
            keyboardType="email-address"
            onChangeText={(text) => this.setState({ email: text })}
            value={this.state.email}
          />
          <TextInput
            style={styles.inputText}
            placeholder="Phone Number"
            onChangeText={(text) => this.setState({ phone: text })}
            value={this.state.phone}
          />
          <TextInput
            style={styles.inputText}
            placeholder="Address"
            onChangeText={(text) => this.setState({ address: text })}
            value={this.state.address}
          />
          <TextInput
            style={styles.inputText}
            placeholder="Service type"
            onChangeText={(text) => this.setState({ serviceType: text })}
            value={this.state.serviceType}
          />
          <TextInput
            style={styles.inputTextPass}
            secureTextEntry={true}
            placeholder="Chose Password"
            onChangeText={(text) => this.setState({ password: text })}
            value={this.state.password}
          />
          <TouchableOpacity
            style={styles.Btns}
            onPress={() => alert(this.state)}
          >
            <Text
              style={{
                color: "white",
                fontSize: 20,
                textAlign: "center",
                fontWeight: "bold",
              }}
            >
              Sign up
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
export default Signup;
const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // padding: 20,
    backgroundColor: "#fff",
    paddingTop: 40,
    alignItems: "center",
    flex: 1,
  },
  inputText: {
    height: 40,
    borderColor: "gray",
    borderWidth: 1.5,
    marginBottom: 10,
    paddingLeft: 8,
    paddingRight: 8,
    borderRadius: 5,
  },
  inputTextPass: {
    height: 40,
    borderColor: "gray",
    borderWidth: 1.5,
    paddingLeft: 8,
    paddingRight: 8,
    borderRadius: 5,
  },
  signinBtn: {
    color: "white",
    backgroundColor: "#04609F",
    borderRadius: 5,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
  },
  Btns: {
    width: 200,
    backgroundColor: "#04609F",
    borderRadius: 5,
    height: 50,
    marginTop: 30,
    justifyContent: "center",
  },
  header: {
    width: "100%",
    height: 60,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 20,
  },
});
