import React from "react";
import { StyleSheet, Text, View, TouchableHighlight } from "react-native";
import { Dimensions, Button, TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";

const Header = ({ name, openDrawer }) => (
  <View style={styles.header}>
    <TouchableOpacity onPress={() => openDrawer()}>
      <Ionicons name="ios-menu" size={32} />
    </TouchableOpacity>
    <Text>{name}</Text>
    <Text style={{ width: 50 }}></Text>
  </View>
);

export default function Main({ navigation }) {
  return (
    <View style={styles.container}>
      <Header name="Main" openDrawer={navigation.openDrawer} />
      <View style={{ padding: 20, marginTop: "20%", width: "100%" }}>
        <TouchableOpacity
          style={styles.Btns}
          onPress={() => navigation.navigate("Login")}
        >
          <Text
            style={{
              color: "white",
              fontSize: 20,
              textAlign: "center",
              fontWeight: "bold",
            }}
          >
            Login
          </Text>
        </TouchableOpacity>
        <Text
          style={{
            marginTop: 10,
            marginBottom: 10,
            textAlign: "center",
          }}
        >
          OR
        </Text>

        <TouchableOpacity
          style={styles.Btns}
          onPress={() => navigation.navigate("Signup")}
        >
          <Text
            style={{
              color: "white",
              fontSize: 20,
              textAlign: "center",
              fontWeight: "bold",
            }}
          >
            Sign up
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    paddingTop: 40,
    alignItems: "center",
    flex: 1,
  },
  Btns: {
    width: "100%",
    backgroundColor: "#04609F",
    borderRadius: 5,
    height: 50,
    justifyContent: "center",
  },
  header: {
    width: "100%",
    height: 60,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 20,
  },
});
