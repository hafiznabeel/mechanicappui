import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { ListItem, Icon, Avatar, Button } from "react-native-elements";
import { Card, Title, Paragraph } from "react-native-paper";
import { Ionicons } from "@expo/vector-icons";

const Header = ({ name, openDrawer }) => (
  <View style={styles.header}>
    <TouchableOpacity onPress={() => openDrawer()}>
      <Ionicons name="ios-menu" size={32} />
    </TouchableOpacity>
    <Text>{name}</Text>
    <Text style={{ width: 50 }}></Text>
  </View>
);

const Cards = () => (
  <Card style={styles.cardcss}>
    <Card.Content>
      <Title style={styles.titleText}>3</Title>
      <Paragraph style={styles.paragraphText}>Pending Order</Paragraph>
    </Card.Content>
  </Card>
);
const list = [
  {
    mechanic: "Hyundai Cert",
    car: "2019",
    address: "Hyundai",
    cost: "Hyundai Cert",
    service: "Airbags are not working properly...",
    avatar_url:
      "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg",
  },
  {
    mechanic: "Hyundai Cert",
    car: "2019",
    address: "Hyundai",
    cost: "Hyundai Cert",
    service: "Airbags are not working properly...",
    avatar_url:
      "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg",
  },
  {
    mechanic: "Hyundai Cert",
    car: "2019",
    address: "Hyundai",
    cost: "Hyundai Cert",
    service: "Airbags are not working properly...",
    avatar_url:
      "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg",
  },
];
function OrderDetails({ navigation }) {
  return (
    <View style={styles.container}>
      <Header name="Monthly Log" openDrawer={navigation.openDrawer} />
      <View style={{ padding: 20, width: "100%" }}>
        <Text style={{ fontSize: 20, color: "gray" }}>Monthly Service Log</Text>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
          }}
        >
          <Cards />
          <Cards />
          <Cards />
        </View>

        {list.map((l, i) => (
          <ListItem key={i} bottomDivider style={{ marginBottom: 10 }}>
            <ListItem.Content>
              <View
                style={{
                  flexDirection: "row",
                }}
              >
                <ListItem.Subtitle>Mechanic: {l.mechanic}</ListItem.Subtitle>
                <ListItem.Subtitle>Car: {l.car}</ListItem.Subtitle>
              </View>

              <ListItem.Subtitle>Address: {l.address}</ListItem.Subtitle>
              <ListItem.Subtitle>Cost: {l.cost}</ListItem.Subtitle>
              <ListItem.Subtitle>service: {l.service}</ListItem.Subtitle>
            </ListItem.Content>
          </ListItem>
        ))}
      </View>
    </View>
  );
}

export default OrderDetails;
const styles = StyleSheet.create({
  container: {
    paddingTop: 40,
    flex: 1,
  },
  cardcss: {
    height: "90%",
    width: "30%",
    padding: 5,
    margin: 8,
  },
  paragraphText: {
    fontSize: 10,
  },
  titleText: {
    alignContent: "center",
  },
  header: {
    width: "100%",
    height: 60,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 20,
  },
});
