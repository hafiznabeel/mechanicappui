import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableOpacity,
} from "react-native";
import { ListItem, Avatar, Icon } from "react-native-elements";
import { Ionicons } from "@expo/vector-icons";

const list = [
  {
    name: "Hyundai Cert",
    year: "2019",
    make: "Hyundai",
    model: "Hyundai Cert",
    engine: "VTI",
    regNo: "LEEN 122",
    avatar_url:
      "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg",
  },
  {
    name: "Hyundai Cert",
    year: "2019",
    make: "Hyundai",
    model: "Hyundai Cert",
    engine: "VTI",
    regNo: "LEEN 122",
    avatar_url:
      "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg",
  },
  {
    name: "Hyundai Cert",
    year: "2019",
    make: "Hyundai",
    model: "Hyundai Cert",
    engine: "VTI",
    regNo: "LEEN 122",
    avatar_url:
      "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg",
  },
];
const Header = ({ name, openDrawer }) => (
  <View style={styles.header}>
    <TouchableOpacity onPress={() => openDrawer()}>
      <Ionicons name="ios-menu" size={32} />
    </TouchableOpacity>
    <Text>{name}</Text>
    <Text style={{ width: 50 }}></Text>
  </View>
);

function ListVehicles({ navigation }) {
  return (
    <View style={styles.container}>
      <Header name="List Vehices" openDrawer={navigation.openDrawer} />
      <View style={{ padding: 20, width: "100%" }}>
        {list.map((l, i) => (
          <ListItem key={i} bottomDivider style={{ marginBottom: 10 }}>
            <Avatar
              source={{ uri: l.avatar_url }}
              style={{ width: "30%", height: "100%" }}
            />
            <ListItem.Content>
              <ListItem.Title>{l.name}</ListItem.Title>
              <ListItem.Subtitle>Year: {l.year}</ListItem.Subtitle>
              <ListItem.Subtitle>Make: {l.make}</ListItem.Subtitle>
              <ListItem.Subtitle>Model: {l.model}</ListItem.Subtitle>
              <ListItem.Subtitle>Engine: {l.engine}</ListItem.Subtitle>
              <ListItem.Subtitle>License No: {l.regNo}</ListItem.Subtitle>
            </ListItem.Content>
            <View style={{ marginTop: "20%" }}>
              <View style={{ flexDirection: "row" }}>
                <Icon name="edit" />
                <Text>Edit</Text>
              </View>

              <View style={{ flexDirection: "row" }}>
                <Icon name="delete" />
                <Text>Remove</Text>
              </View>
            </View>
          </ListItem>
        ))}
      </View>
    </View>
  );
}

export default ListVehicles;
const styles = StyleSheet.create({
  container: {
    paddingTop: 40,
    flex: 1,
  },
  signinBtn: {
    color: "white",
    backgroundColor: "#04609F",
    borderRadius: 5,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
  },
  header: {
    width: "100%",
    height: 60,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 20,
  },
});
